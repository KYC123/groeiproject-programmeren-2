package be.kdg.whiskyproject.model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class WhiskyTest {
    private static Whisky w1;
    private static Whisky w2;
    private static Whisky w3;

    @Before
    public void setUp() throws Exception {
        w1 = new Whisky("Whisky1", 40, 4, LocalDate.parse("2000-01-01"), "Scotland", WhiskyType.MALT);
        w2 = new Whisky("Whisky2", 40, 8, LocalDate.parse("1901-01-01"), "Japan" ,WhiskyType.GRAIN);
        w3 = new Whisky("Whisky3", 100, 16, LocalDate.parse("1750-01-01"), "United States", WhiskyType.BLENDED);
    }

    @Test
    public void testEquals() {
        assertNotEquals(w1, w2, "De whiskies moeten verschillen");
        assertNotEquals(w1, w3, "De whiskies moeten verschillen");
        assertNotEquals(w2, w, "De whiskies moeten verschillen");
        Whisky w4 = new Whisky("Whisky1",0,5 ,LocalDate.parse("2010-01-01"),"Ireland", WhiskyType.BOURBON);
        assertEquals(w1, w4,
                () -> String.format("De naam van beide whiskies moeten hetzelfde zijn. w1=\"%s\" w2=\"%s\"",w1.getName(), w4.getName()));
    }

    @Test
    public void testIllegalDate(){
        assertThrows(IllegalArgumentException.class, () -> w1.setDateCreated(LocalDate.parse("2023-01-01")), "Een illegale datum moet een IllegalArgumentException throwen");
        try {
            assertDoesNotThrow(() -> w1.setDateCreated(LocalDate.parse("2000-01-01")), "Een juiste datum mag geen exception throwen");
        } catch (IllegalArgumentException e) {
            fail("Er wordt een IllegalArgumentException gethrowed terwijl de datum juist is",e);
        }
    }

    @Test
    public void testCompareTo(){
        assertNotEquals(0, w1.compareTo(w),"De compareTo mag geen 0 geven want de naam van de whiskies verschilt");
        assertEquals(0, w1.compareTo(new Whisky("Whisky1",200,20,LocalDate.parse("2000-01-01"),"Scotland",WhiskyType.BLENDED)), "De compareTo moet 0 geven");
    }

}