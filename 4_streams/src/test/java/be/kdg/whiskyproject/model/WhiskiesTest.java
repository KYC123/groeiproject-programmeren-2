package be.kdg.whiskyproject.model;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class WhiskiesTest {
    private static Whiskies whiskies1;
    private static Whisky w1;
    private static Whisky w2;
    private static Whisky w3;

    @BeforeEach
    void setUp(){
        whiskies1 = new Whiskies();
        w1 = new Whisky("Alpha", 20, 10, LocalDate.parse("1998-01-01"), "Scotland", WhiskyType.BLENDED);
        w2 = new Whisky("Beta", 10, 20, LocalDate.parse("2001-01-01"), "Ireland", WhiskyType.GRAIN);
        w3 = new Whisky("Charlie", 30, 30, LocalDate.parse("2000-01-01"), "United States", WhiskyType.RYE);
    }

    @Test
    public void testAdd(){
        assertTrue(whiskies1.add(w1), "De whisky die toegevoegd wordt moet uniek zijn");
        assertFalse(whiskies1.add(w1), "De whisky die toegevoegd wordt moet uniek zijn");
    }

    @Test
    public void testRemove(){
        whiskies1.add(w1);
        assertTrue(whiskies1.remove("Alpha"), "de whisky moet bestaan in de list");
        assertFalse(whiskies1.remove("Alpha"), "de whisky moet bestaan in de list");
    }

    @Test
    public void testYearsAgedSort(){
        whiskies1.add(w1);
        whiskies1.add(w2);
        whiskies1.add(w3);
        assertAll(
                () -> assertEquals(10, whiskies1.sortedBy(Whisky::getYearsAged).get(0).getYearsAged()),
                () -> assertEquals(20, whiskies1.sortedBy(Whisky::getYearsAged).get(1).getYearsAged()),
                () -> assertEquals(30, whiskies1.sortedBy(Whisky::getYearsAged).get(2).getYearsAged())
        );
    }

    @Test
    public void testPriceSort(){
        whiskies1.add(w1);
        whiskies1.add(w2);
        whiskies1.add(w3);
        Object[] expected = {w2, w1, w3};
        Object[] sortedArray = whiskies1.sortedBy(Whisky::getAveragePrice).toArray();
        assertArrayEquals(expected, sortedArray, "De arrays moeten hetzelfde zijn");
    }

}