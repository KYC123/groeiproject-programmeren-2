package be.kdg.whiskyproject.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.function.Function;

public class Whiskies {
    private TreeSet<Whisky> whiskies = new TreeSet<>();

    public boolean add(Whisky whisky){
        try{
            whiskies.add(whisky);
            return true;
        }
        catch (Exception e){
            System.out.println("De whisky die je probeerde toe te voegen is niet uniek.");
            return false;
        }
    }

    public boolean remove(String name){
        try {
            whiskies.removeIf(whisky -> whisky.getName().equals(name)); //dit is een verkorte versie van een for-each loop die doorheen de set loopt
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public Whisky search(String name){
        for (Whisky whisky : whiskies) {
            if (whisky.getName().equals(name)){
                return whisky;
            }
        }
        return null;
    }

    public List<Whisky> sortedBy(Function<Whisky, Comparable> f){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(Comparator.comparing(f));
        return list;
    }

    public int getSize(){
        return whiskies.size();
    }

}
