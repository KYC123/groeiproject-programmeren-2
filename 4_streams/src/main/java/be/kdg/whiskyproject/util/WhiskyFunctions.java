package be.kdg.whiskyproject.util;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class WhiskyFunctions {
    public static <T> List<T> filteredList(List<T> packList, Predicate<T> predicate){
        return packList.stream().filter(predicate).collect(Collectors.toList());
    }

    public static <T> Double average(List<T> packList, ToDoubleFunction<T> mapper){
        return packList.stream().mapToDouble(mapper).average().getAsDouble();
    }

    public static <T> long countIf(List<T> packList, Predicate<T> predicate){
        return packList.stream().filter(predicate).count();
    }
}
