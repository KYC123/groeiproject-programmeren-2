import be.kdg.whiskyproject.data.Data;
import be.kdg.whiskyproject.model.Whiskies;
import be.kdg.whiskyproject.model.Whisky;
import be.kdg.whiskyproject.model.WhiskyType;
import be.kdg.whiskyproject.util.WhiskyFunctions;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Demo_4 {
    public static void main(String[] args){
        Whiskies whiskies = new Whiskies();
        Data.getData().forEach(whiskies::add);
        System.out.println("\nWhiskies gesorteerd op naam:");
        whiskies.sortedBy(Whisky::getName).forEach(System.out::println);

        List<Whisky> whiskyList = Data.getData();
        whiskyList = WhiskyFunctions.filteredList(whiskyList, p -> p.getType().equals(WhiskyType.MALT));
        whiskyList = WhiskyFunctions.filteredList(whiskyList, p -> p.getAveragePrice() < 100);
        whiskyList = WhiskyFunctions.filteredList(whiskyList, p -> p.getYearsAged() > 4);
        System.out.println("\nToepassing 3 keer filteredList met telkens een ander Predicate:" +
                "\n Alle whiskies die single malt zijn, minder dan 100 euro kosten en meer dan 4 jaar gerijpt zijn:");
        whiskyList.forEach(System.out::println);

        whiskyList = Data.getData();
        System.out.printf("\nGemiddelde prijs van alle whiskies: %.2f euro\n", WhiskyFunctions.average(whiskyList, Whisky::getAveragePrice));
        System.out.printf("Aantal whiskies van het \"single malt\" type: %d\n",WhiskyFunctions.countIf(whiskyList, p -> p.getType().equals(WhiskyType.MALT)));

        System.out.printf("Aantal whiskies die voor 1900 uitgebracht zijn: %d\n", whiskyList.stream().filter(p -> p.getDateCreated().isBefore(LocalDate.of(1900,1,1))).count());
        System.out.println("\nAlle whiskies gesorteerd op type en dan op naam:");
        whiskyList.stream().sorted(Comparator.comparing(Whisky::getType).thenComparing(Whisky::getName)).forEach(System.out::println);
        System.out.println("\nAlle namen in hoofdletters, omgekeerd gesorteerd en zonder dubbels:");
        System.out.println(whiskyList.stream().map(Whisky::getName).distinct().map(String::toUpperCase).sorted(Comparator.reverseOrder()).collect(Collectors.joining(", ")));
        System.out.println("\nEen willekeurige whisky die meer dan 40 euro kost:");
        whiskyList.stream().filter(p -> p.getAveragePrice()>40).findAny().ifPresentOrElse(p -> System.out.println(p.getName() +" : "+p.getAveragePrice()),
                () -> System.out.println("Geen whisky gevonden die aan de voorwaardes voldoen"));
        System.out.printf("\nDe whisky die het langst gerijpt wordt: %s\n", whiskyList.stream().max(Comparator.comparing(Whisky::getYearsAged)).map(Whisky::getName).get());
        System.out.println("\nList met gesorteerde namen van whiskies die geen l bevatten:");
        System.out.println(whiskyList.stream().filter(p -> !(p.getName().contains("l"))).map(Whisky::getName).sorted().collect(Collectors.joining(", ","[","]")));
        Map<Boolean, List<Whisky>> map = whiskyList.stream().collect(Collectors.partitioningBy(p -> p.getLandOfOrigin().equals("Scotland")));
        System.out.println("\nSublist met whiskies die van Schotland komen:");
        map.get(true).forEach(System.out::println);
        System.out.println("\nSublist met whiskies die NIET van Schotland komen:");
        map.get(false).forEach(System.out::println);
    }
}
