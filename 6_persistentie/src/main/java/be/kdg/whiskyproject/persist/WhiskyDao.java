package be.kdg.whiskyproject.persist;

import be.kdg.whiskyproject.model.Whisky;

import java.util.List;

public interface WhiskyDao {
    boolean insert(Whisky w);
    boolean delete(String name);
    boolean update(Whisky w);
    Whisky retrieve(String name);
    List<Whisky> sortedOn(String query);
}
