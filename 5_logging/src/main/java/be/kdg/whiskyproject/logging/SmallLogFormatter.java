package be.kdg.whiskyproject.logging;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SmallLogFormatter extends Formatter {
    @Override
    public String format(LogRecord logRecord){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.SSS");
        LocalDateTime date = Instant.ofEpochMilli(logRecord.getMillis()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String logMessage = MessageFormat.format(logRecord.getMessage(), logRecord.getParameters());
        return dateTimeFormatter.format(date)+" Level: "+logRecord.getLevel()+" melding: \""+logMessage+"\"\n";
    }
}
