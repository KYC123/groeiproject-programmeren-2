package be.kdg.whiskyproject.model;

import java.time.LocalDate;
import java.util.Objects;
import java.util.logging.Logger;

public class Whisky implements Comparable<Whisky>{
    private String name;
    private double averagePrice;
    private int yearsAged;
    private LocalDate dateCreated;
    private String landOfOrigin;
    private WhiskyType type;
    private static final Logger logger = Logger.getLogger(Whisky.class.getName());

    public Whisky() {
        this("Ongekend", 0, 0, LocalDate.parse("2000-01-01"), "Ongekend", WhiskyType.MALT);
    }

    public Whisky(String name, double averagePrice, int yearsAged, LocalDate dateCreated, String landOfOrigin, WhiskyType type) {
        setName(name);
        setAveragePrice(averagePrice);
        setYearsAged(yearsAged);
        setDateCreated(dateCreated);
        setLandOfOrigin(landOfOrigin);
        setType(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.equals("")){
            logger.severe(String.format("De naam mag niet leeg zijn. Gegeven naam van %s: %s", this.name, name));
        }
        else this.name = name;
    }

    public double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        if (averagePrice<0 || averagePrice>35000){
            logger.severe(String.format("De prijs moet tussen 0 en 35000 liggen. Gegeven prijs van %s: %.2f", this.name, averagePrice));
        }
        else this.averagePrice = averagePrice;
    }

    public int getYearsAged() {
        return yearsAged;
    }

    public void setYearsAged(int yearsAged) {
        if (yearsAged < 0 || yearsAged > 100) {
            logger.severe(String.format("Dit getal moet tussen de 0 en 100 liggen. Gegeven jaren rijping %s: %d", this.name, yearsAged));
        }
        else this.yearsAged = yearsAged;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        if (dateCreated.isBefore(LocalDate.parse("0000-01-01")) || dateCreated.isAfter(LocalDate.now())){
            logger.severe(String.format("Deze datum klopt niet. Gegeven datum van %s: %s", this.name, dateCreated.toString()));
        }
        else this.dateCreated = dateCreated;
    }

    public String getLandOfOrigin() {
        return landOfOrigin;
    }

    public void setLandOfOrigin(String landOfOrigin) {
        if (landOfOrigin.equals("")){
            logger.severe(String.format("De naam mag niet leeg zijn. Gegeven naam van %s: %s", this.name, landOfOrigin));
        }
        else this.landOfOrigin = landOfOrigin;
    }

    public WhiskyType getType() {
        return type;
    }

    public void setType(WhiskyType type) {
        try {
            this.type = type;
        } catch (IllegalArgumentException e){
            logger.severe(String.format("Gelieve een geldig type te kiezen. Gegeven type van %s: %s", this.name, type.name()));
        }
    }

    @Override
    public int compareTo(Whisky o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Whisky whisky = (Whisky) o;
        return Objects.equals(this.name, whisky.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    @Override
    public String toString() {
        return String.format("%s %d years\ttype: %s\tland of origin: %s\tdate created: %tF\tavg price: %f euro",name,yearsAged,type.name(),landOfOrigin,dateCreated,averagePrice);
    }
}
