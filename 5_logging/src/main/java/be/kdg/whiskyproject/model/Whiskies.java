package be.kdg.whiskyproject.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Logger;

public class Whiskies {
    private TreeSet<Whisky> whiskies = new TreeSet<>();
    private static final Logger logger = Logger.getLogger(Whisky.class.getName());

    public boolean add(Whisky whisky){
        logger.finer("Object toegevoegd aan treeset: "+whisky.getName());
        return whiskies.add(whisky);

    }

    public boolean remove(String name){
        logger.finer("Object verwijderd uit treeset: "+name);
        return whiskies.removeIf(whisky -> whisky.getName().equals(name)); //dit is een verkorte versie van een for-each loop die doorheen de set loopt
    }

    public Whisky search(String name){
        for (Whisky whisky : whiskies) {
            if (whisky.getName().equals(name)){
                return whisky;
            }
        }
        return null;
    }

    public List<Whisky> sortedOnName(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }

    public List<Whisky> sortedOnPrice(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return Double.compare(o1.getAveragePrice(),o2.getAveragePrice());
            }
        });
        return list;
    }

    public List<Whisky> sortedOnDateCreated(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return o1.getDateCreated().compareTo(o2.getDateCreated());
            }
        });
        return list;
    }

    public int getSize(){
        return whiskies.size();
    }

}
