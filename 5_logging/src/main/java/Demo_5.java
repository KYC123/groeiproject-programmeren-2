import be.kdg.whiskyproject.model.Whiskies;
import be.kdg.whiskyproject.model.Whisky;
import be.kdg.whiskyproject.model.WhiskyType;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;

public class Demo_5 {
    public static void main(String[] args){
        URL configURL = Demo_5.class.getResource("/logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }

        Whisky w1 = new Whisky("test1",10,4, LocalDate.parse("2024-01-01"),"Scotland", WhiskyType.BLENDED);
        Whisky w2 = new Whisky("test2",-10,4, LocalDate.parse("2019-01-01"),"Ireland", WhiskyType.BLENDED);
        Whisky w3 = new Whisky("test3",10,4, LocalDate.parse("2019-01-01"),"", WhiskyType.BLENDED);
        Whiskies whiskies = new Whiskies();
        whiskies.add(w1);
        whiskies.remove("test1");
    }
}
