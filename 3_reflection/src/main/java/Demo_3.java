import be.kdg.whiskyproject.model.Drink;
import be.kdg.whiskyproject.model.Whiskies;
import be.kdg.whiskyproject.model.Whisky;
import be.kdg.whiskyproject.reflection.ReflectionTools;

import java.lang.reflect.InvocationTargetException;

public class Demo_3 {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ReflectionTools.classAnalysis(Drink.class, Whisky.class, Whiskies.class);
        Object o = ReflectionTools.runAnnotated(Drink.class); //ik moet de superklasse gebruiken want in de basisklasse zitten geen setters die een String als parameter hebben.
        System.out.println("Aangemaakt object door runAnnotated:");
        System.out.println(o.toString());
    }
}
