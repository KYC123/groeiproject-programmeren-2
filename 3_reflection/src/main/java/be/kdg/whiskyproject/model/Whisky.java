package be.kdg.whiskyproject.model;

import java.time.LocalDate;

public class Whisky extends Drink {
    private int yearsAged;
    private WhiskyType type;

    public Whisky() {
        super();
        this.yearsAged = 0;
        this.type = WhiskyType.MALT;
    }

    public Whisky(String name, double averagePrice, int yearsAged, LocalDate dateCreated, String landOfOrigin, WhiskyType type) {
        super(name, averagePrice, dateCreated, landOfOrigin);
        this.yearsAged = yearsAged;
        this.type = type;
    }

    public int getYearsAged() {
        return yearsAged;
    }

    public void setYearsAged(int yearsAged) {
        if (yearsAged < 0 || yearsAged > 100) {
            throw new IllegalArgumentException("Dit getal moet tussen de 0 en 100 liggen!");
        }
        else this.yearsAged = yearsAged;
    }

    public WhiskyType getType() {
        return type;
    }

    public void setType(WhiskyType type) {
        try {
            this.type = type;
        } catch (IllegalArgumentException e){
            System.out.println("Gelieve een geldig type te kiezen.");
        }
    }

    @Override
    public String toString() {
        return String.format("%s %d years\ttype: %s\tland of origin: %s\tdate created: %tF\tavg price: %f euro",name,yearsAged,type.name(),landOfOrigin,dateCreated,averagePrice);
    }
}
