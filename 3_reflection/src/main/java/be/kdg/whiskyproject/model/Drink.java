package be.kdg.whiskyproject.model;

import be.kdg.whiskyproject.reflection.CanRun;

import java.time.LocalDate;
import java.util.Objects;

public class Drink {
    protected String name;
    protected double averagePrice;
    protected LocalDate dateCreated;
    protected String landOfOrigin;

    public Drink(){
        this.name = "Ongekend";
        this.averagePrice = 0;
        this.dateCreated = LocalDate.parse("2000-01-01");
        this.landOfOrigin = "Ongekend";
    }

    public Drink(String name, double averagePrice, LocalDate dateCreated, String landOfOrigin) {
        this.name = name;
        this.averagePrice = averagePrice;
        this.dateCreated = dateCreated;
        this.landOfOrigin = landOfOrigin;
    }

    public String getName() {
        return name;
    }

    @CanRun("Suntory")
    public void setName(String name) {
        if (name.equals("")){
            throw new IllegalArgumentException("De naam mag niet leeg zijn.");
        }
        else this.name = name;
    }

    public double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        if (averagePrice<0 || averagePrice>35000){
            throw new IllegalArgumentException("De prijs moet tussen 0 en 35000 liggen.");
        }
        else this.averagePrice = averagePrice;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        if (dateCreated.isBefore(LocalDate.parse("0000-01-01")) || dateCreated.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("Deze datum klopt niet.");
        }
        else this.dateCreated = dateCreated;
    }

    public String getLandOfOrigin() {
        return landOfOrigin;
    }

    public void setLandOfOrigin(String landOfOrigin) {
        if (landOfOrigin.equals("")){
            throw new IllegalArgumentException("Het land mag niet blanco zijn.");
        }
        else this.landOfOrigin = landOfOrigin;
    }

    public int compareTo(Whisky o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Whisky whisky = (Whisky) o;
        return Objects.equals(this.name, whisky.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    @Override
    public String toString(){
        return String.format("%s\tavg price: %.2f\tdate created: %tF\tland of origin: %s", name, averagePrice, dateCreated, landOfOrigin);
    }
}
