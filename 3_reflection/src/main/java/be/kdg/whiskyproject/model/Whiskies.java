package be.kdg.whiskyproject.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class Whiskies {
    private TreeSet<Whisky> whiskies = new TreeSet<>();

    public boolean add(Whisky whisky){
        try{
            whiskies.add(whisky);
            return true;
        }
        catch (Exception e){
            System.out.println("De whisky die je probeerde toe te voegen is niet uniek.");
            return false;
        }
    }

    public boolean remove(String name){
        try {
            whiskies.removeIf(whisky -> whisky.getName().equals(name)); //dit is een verkorte versie van een for-each loop die doorheen de set loopt
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public Whisky search(String name){
        for (Whisky whisky : whiskies) {
            if (whisky.getName().equals(name)){
                return whisky;
            }
        }
        return null;
    }

    public List<Whisky> sortedOnName(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return list;
    }

    public List<Whisky> sortedOnPrice(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return Double.compare(o1.getAveragePrice(),o2.getAveragePrice());
            }
        });
        return list;
    }

    public List<Whisky> sortedOnDateCreated(){
        List<Whisky> list = new ArrayList<>(whiskies);
        list.sort(new Comparator<Whisky>() {
            @Override
            public int compare(Whisky o1, Whisky o2) {
                return o1.getDateCreated().compareTo(o2.getDateCreated());
            }
        });
        return list;
    }

    public int getSize(){
        return whiskies.size();
    }

}
