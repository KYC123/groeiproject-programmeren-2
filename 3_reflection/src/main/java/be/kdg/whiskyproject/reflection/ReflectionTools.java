package be.kdg.whiskyproject.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTools {
    public static void classAnalysis(Class... c){
        for (Class aClass: c) {
            System.out.println("Analyse van de klasse: " + aClass.getSimpleName());
            System.out.println("================================================");
            System.out.println("Fully qualified name\t: " + aClass.getName());
            System.out.println("Naam van de superklasse\t: " + aClass.getSuperclass().getSimpleName());
            System.out.println("Naam van de package\t: " + aClass.getPackageName());
            System.out.print("Interfaces: ");
            for (Class cl : aClass.getInterfaces()) {
                System.out.print(cl.getSimpleName() + " ");
            }
            System.out.println("\nConstructors:");
            for (Constructor con : aClass.getDeclaredConstructors()) {
                System.out.print("\t" + con + "\n");
            }
            System.out.print("attributen\t: ");
            for (Field f : aClass.getDeclaredFields()) {
                System.out.print(f.getName() + " ");
            }
            System.out.print("\ngetters\t\t: ");
            for (Method m : aClass.getDeclaredMethods()) {
                if (m.getName().startsWith("get")) System.out.print(m.getName() + " ");
            }
            System.out.print("\nsetters\t\t: ");
            for (Method m : aClass.getDeclaredMethods()) {
                if (m.getName().startsWith("set")) System.out.print(m.getName() + " ");
            }
            System.out.print("\nandere methoden : ");
            for (Method m : aClass.getDeclaredMethods()) {
                if (!(m.getName().startsWith("get")) && !(m.getName().startsWith("set")))
                    System.out.print(m.getName() + " ");
            }
            System.out.println();
            System.out.println();
        }
    }

    public static Object runAnnotated(Class aClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object o = aClass.getDeclaredConstructor().newInstance();
        for (Method m: aClass.getDeclaredMethods()) {
            if (m.isAnnotationPresent(CanRun.class) && m.getParameterTypes()[0].equals(String.class)){ //check of de CanRun annotation er is en of het parameter type een String is
                m.invoke(o,m.getAnnotation(CanRun.class).value());
            }
        }
        return o;
    }
}
