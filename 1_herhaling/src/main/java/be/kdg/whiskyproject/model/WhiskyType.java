package be.kdg.whiskyproject.model;

public enum WhiskyType {
    MALT, GRAIN, BLENDED, BOURBON, RYE, POT_STILL
}
