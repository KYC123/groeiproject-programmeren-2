import be.kdg.whiskyproject.data.Data;
import be.kdg.whiskyproject.model.Whiskies;
import be.kdg.whiskyproject.model.Whisky;

import java.util.List;

public class Demo_1 {
    public static void main(String[] args) {
        System.out.println("gradle working");
        Whiskies whiskies = new Whiskies(); //instantie multiklasse
        List<Whisky> datalist = Data.getData(); //gevulde datalist
        for (Whisky w: datalist) { //objecten toevoegen aan multiklasse
            whiskies.add(w);
        }
        whiskies.add(new Whisky()); //dubbel object toevoegen
        System.out.println("search op Bushmills: "+whiskies.search("Bushmills")); //search
        whiskies.remove("Ongekend"); //remove
        whiskies.add(new Whisky());
        System.out.println("size: "+whiskies.getSize()); //getSize
        System.out.println("---------------------");
        System.out.println("Sorted by name: ");
        for (Whisky w : whiskies.sortedOnName()) {
            System.out.println(w.toString());
        }
        System.out.println("---------------------");
        System.out.println("Sorted by date created: ");
        for (Whisky w : whiskies.sortedOnDateCreated()) {
            System.out.println(w.toString());
        }
        System.out.println("---------------------");
        System.out.println("Sorted by average price: ");
        for (Whisky w : whiskies.sortedOnPrice()) {
            System.out.println(w.toString());
        }

    }
}
