package be.kdg.whiskyproject.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Klasse Whisky.
 * Beschrijft een whisky met variabelen.
 *
 * @author Kamiel Ceuppens
 * @version 1.0, 10 jan 2022
 * @see <a href="https://whizzky.net/index">Meer informatie over verschillende soorten whisky</a>
 */

public class Whisky implements Comparable<Whisky>{
    private String name;
    private double averagePrice;
    private int yearsAged;
    private LocalDate dateCreated;
    private String landOfOrigin;
    private WhiskyType type;

    /**
     * Constructor van een Whisky met standaard attributen.
     */
    public Whisky() {
        this("Ongekend", 0, 0, LocalDate.parse("2000-01-01"), "Ongekend", WhiskyType.MALT);
    }

    /**
     * Constructor van een Whisky met doorgegeven attributen.
     *
     * @param name naam van de whisky
     * @param averagePrice gemiddelde prijs per fles
     * @param yearsAged hoe lang wordt de whisky gerijpt
     * @param dateCreated datum waarop de whisky is uitgebracht
     * @param landOfOrigin land waar de whisky gemaakt wordt
     * @param type wat voor type whisky het is
     */
    public Whisky(String name, double averagePrice, int yearsAged, LocalDate dateCreated, String landOfOrigin, WhiskyType type) {
        setName(name);
        setAveragePrice(averagePrice);
        setYearsAged(yearsAged);
        setDateCreated(dateCreated);
        setLandOfOrigin(landOfOrigin);
        setType(type);
    }

    /**
     * Return naam van de whisky.
     * @return naam van de whisky
     */
    public String getName() {
        return name;
    }

    /**
     * Set naam van de whisky.
     * @param name naam whisky
     */
    public void setName(String name) {
        if (name.equals("")){
            throw new IllegalArgumentException("De naam mag niet leeg zijn.");
        }
        else this.name = name;
    }

    /**
     * Return gemiddelde prijs van de whisky.
     * @return gemiddelde prijs van de whisky
     */
    public double getAveragePrice() {
        return averagePrice;
    }

    /**
     * Set gemiddelde prijs van de whisky.
     * @param averagePrice gemiddelde prijs
     */
    public void setAveragePrice(double averagePrice) {
        if (averagePrice<0 || averagePrice>35000){
            throw new IllegalArgumentException("De prijs moet tussen 0 en 35000 liggen.");
        }
        else this.averagePrice = averagePrice;
    }

    /**
     * Return het aantal jaren dat de whisky gerijpt wordt.
     * @return jaren whisky gerijpt
     */
    public int getYearsAged() {
        return yearsAged;
    }

    /**
     * Set jaren whisky gerijpt.
     * @param yearsAged jaren whisky gerijpt
     */
    public void setYearsAged(int yearsAged) {
        if (yearsAged < 0 || yearsAged > 100) {
            throw new IllegalArgumentException("Dit getal moet tussen de 0 en 100 liggen!");
        }
        else this.yearsAged = yearsAged;
    }

    /**
     * Return datum creatie whisky..
     * @return datum creatie whisky
     */
    public LocalDate getDateCreated() {
        return dateCreated;
    }

    /**
     * Set datum creatie whisky.
     * @param dateCreated datum creatie whisky
     */
    public void setDateCreated(LocalDate dateCreated) {
        if (dateCreated.isBefore(LocalDate.parse("0000-01-01")) || dateCreated.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("Deze datum klopt niet.");
        }
        else this.dateCreated = dateCreated;
    }

    /**
     * Return het land waar de whisky gemaakt wordt.
     * @return land van oorsprong whisky
     */
    public String getLandOfOrigin() {
        return landOfOrigin;
    }

    /**
     * Set land van oorsprong whisky.
     * @param landOfOrigin land van oorsprong
     */
    public void setLandOfOrigin(String landOfOrigin) {
        if (landOfOrigin.equals("")){
            throw new IllegalArgumentException("Het land mag niet blanco zijn.");
        }
        else this.landOfOrigin = landOfOrigin;
    }

    /**
     * Return het type whisky.
     * @return type whisky
     * @see be.kdg.whiskyproject.model.WhiskyType enum WhiskyType
     */
    public WhiskyType getType() {
        return type;
    }

    /**
     * Set whisky type.
     * @param type type whisky
     * @see be.kdg.whiskyproject.model.WhiskyType enum WhiskyType
     */
    public void setType(WhiskyType type) {
        try {
            this.type = type;
        } catch (IllegalArgumentException e){
            System.out.println("Gelieve een geldig type te kiezen.");
        }
    }

    /**
     * Vergelijkt de namen van 2 whiskys
     * @param o de whisky die je wilt vergelijken
     * @return true als de namen hetzelfde zijn, anders false
     */
    @Override
    public int compareTo(Whisky o) {
        return this.name.compareTo(o.name);
    }

    /**
     * Vergelijkt 2 whiskys en returnt of ze gelijk zijn
     * @param o de whisky die je wilt vergelijken
     * @return true als de whiskys gelijk zijn, anders false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Whisky whisky = (Whisky) o;
        return Objects.equals(this.name, whisky.name);
    }

    /**
     * Override van de hashCode() methode van Comparable
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    /**
     * Override van de toString() methode om alle attributen mooi weer te geven
     * @return informatie over een whisky
     */
    @Override
    public String toString() {
        return String.format("%s %d years\ttype: %s\tland of origin: %s\tdate created: %tF\tavg price: %f euro",name,yearsAged,type.name(),landOfOrigin,dateCreated,averagePrice);
    }
}
