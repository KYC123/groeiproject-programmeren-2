package be.kdg.whiskyproject.generics;

import java.util.*;

public class PriorityQueue<T> implements FIFOQueue<T>{
    private SortedMap<Integer, LinkedList<T>> map;

    public PriorityQueue(){
        this.map = new TreeMap<>(Collections.reverseOrder()); //treemap met keys in dalende volgorde
    }

    @Override
    public boolean enqueue(T element, int priority) {
        if (priority<1||priority>5) return false;
        for (Map.Entry<Integer, LinkedList<T>> entry: map.entrySet()) { //keySet() of values() zou ook kunnen
            if (entry.getValue().contains(element)) return false;
        }
        if (map.containsKey(priority)) { //als de map al de gegeven priority bevat, betekent dit dat er ook al een lijst gelinkt aan die priority bestaat.
            map.get(priority).add(element);
        }
        else { //anders voeg je de priority toe samen met een nieuwe LinkedList
            LinkedList<T> list = new LinkedList<>();
            list.add(element);
            map.put(priority, list);
        }
        return true;
    }

    @Override
    public T dequeue() {
        T t = map.get(map.firstKey()).removeFirst(); //eerste object in linkedlist met hoogste priority
        if (map.get(map.firstKey()).isEmpty()) map.remove(map.firstKey()); //remove key-value paar als list leeg is
        return t;
    }

    @Override
    public int search(T element) {
        int pos = 0;
        for (Integer key : map.keySet()) {
            for (T t: map.get(key)) {
                pos++;
                if (t.equals(element)) return pos;
            }
        }
        return -1;
    }

    @Override
    public int getSize() {
        int size = 0;
        for (Integer key : map.keySet()) { //values() of entrySet() zou ook kunnen
            size+=map.get(key).size();
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Integer key : map.keySet()) {
            for (T t: map.get(key)) {
                s.append(key).append(": ").append(t.toString()).append("\n");
            }
        }
        return s.toString();
    }
}
