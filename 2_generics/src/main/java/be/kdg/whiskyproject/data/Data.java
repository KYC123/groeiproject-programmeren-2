package be.kdg.whiskyproject.data;

import be.kdg.whiskyproject.model.Whisky;
import be.kdg.whiskyproject.model.WhiskyType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Data {
    public static List<Whisky> getData(){
        List<Whisky> list = new ArrayList<>();
        list.add(new Whisky());
        list.add(new Whisky("Jack Daniels Old No. 7", 22, 4, LocalDate.parse("1866-01-01"), "United States", WhiskyType.BOURBON));
        list.add(new Whisky("Chivas Regal", 44, 12, LocalDate.parse("1909-01-01"), "Scotland", WhiskyType.BLENDED));
        list.add(new Whisky("Johnnie Walker Black Label", 26, 12, LocalDate.parse("1901-06-01"), "Scotland", WhiskyType.BLENDED));
        list.add(new Whisky("Johnnie Walker Red Label", 15, 4, LocalDate.parse("1901-06-01"), "Scotland", WhiskyType.BLENDED));
        list.add(new Whisky("Macallan 15", 118, 15, LocalDate.parse("1824-01-01"), "Scotland", WhiskyType.MALT));
        list.add(new Whisky("Bushmills", 26, 10, LocalDate.parse("1608-01-01"), "Ireland", WhiskyType.MALT));
        list.add(new Whisky("Angels Envy", 82, 2, LocalDate.parse("1847-01-01"), "United States", WhiskyType.RYE));
        list.add(new Whisky("Suntory The Chita", 45, 4, LocalDate.parse("1899-02-01"), "Japan", WhiskyType.GRAIN));
        list.add(new Whisky("Redbreast 12", 47, 12, LocalDate.parse("1857-01-01"), "Ireland", WhiskyType.POT_STILL));
        return list;
    }
}
