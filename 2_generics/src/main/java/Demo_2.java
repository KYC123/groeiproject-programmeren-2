import be.kdg.whiskyproject.data.Data;
import be.kdg.whiskyproject.generics.PriorityQueue;

import java.util.Random;

public class Demo_2 {
    public static void main(String[] args) {
        var myQueue = new PriorityQueue<>();
        myQueue.enqueue("Tokio", 2);
        myQueue.enqueue("Denver", 5);
        myQueue.enqueue("Rio", 2);
        myQueue.enqueue("Oslo", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van Tokio: " + myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: " + myQueue.search("Nairobi"));
        for(var i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());

        var myQueue2 = new PriorityQueue<>();
        var r = new Random();
        for (var pack: Data.getData()) {
            myQueue2.enqueue(pack, r.nextInt(5)+1);
        }
        System.out.println("\nOverzicht van de PriorityQueue:");
        System.out.println(myQueue2.toString());
        System.out.println("aantal: " + myQueue2.getSize());
        System.out.println("positie van Macallan 15: " + myQueue2.search(Data.getData().get(5)));
        for(var pack: Data.getData()) {
            System.out.println("Dequeue: " + myQueue2.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue2.getSize());
    }
}
